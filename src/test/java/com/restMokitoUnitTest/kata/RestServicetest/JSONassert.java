package com.restMokitoUnitTest.kata.RestServicetest;

import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

public class JSONassert {

    String json_String = "{\"id\":1,\"name\":\"Item 1\",\"price\":10,\"quantity\":100}";

    @Test
    public void testJsonAssert() {

        String expected_json = "{id: 1,name:\"Item 1\"}";  // if Space Use escape character
        try {
            JSONAssert.assertEquals(expected_json, json_String,false);
            // JSONAssert Strict: True, requires all the parameters in the JSON object except space
            // JSONAssert Strict: False, not requires all the paramertes in the JSON Object
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
