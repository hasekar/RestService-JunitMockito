package com.restMokitoUnitTest.kata.RestServicetest.service;

import com.restMokitoUnitTest.kata.RestServicetest.DAO.ItemDAOModel;
import com.restMokitoUnitTest.kata.RestServicetest.Entity.Item;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.beans.Transient;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ItemServiceTest {

    @InjectMocks
    ItemService itemService;

    @Mock
    ItemDAOModel itemDAOModel;

    @Test
    public void testRetriveAllinService() throws Exception {
        when(itemDAOModel.findAll()).thenReturn(Arrays.asList(new Item(100,"item1",10,10)));

        List<Item> actual = itemService.retriveAllItemfromDAO();

        assertEquals(10,actual.get(0).getValue());


    }
}
