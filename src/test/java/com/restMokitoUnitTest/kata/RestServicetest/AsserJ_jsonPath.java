package com.restMokitoUnitTest.kata.RestServicetest;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AsserJ_jsonPath {

    String jsonResponse = "[{\"id\":1001,\"name\":\"iTEM 1\",\"price\":10,\"quantity\":8,\"value\":0},{\"id\":1002,\"name\":\"iTEM 2\",\"price\":20,\"quantity\":6,\"value\":0},{\"id\":1003,\"name\":\"iTEM 3\",\"price\":30,\"quantity\":4,\"value\":0},{\"id\":1004,\"name\":\"iTEM 4\",\"price\":40,\"quantity\":2,\"value\":0}]";

    @Test
    public void jsonPathTest() {
        DocumentContext context = JsonPath.parse(jsonResponse);
        int length = context.read("$.length()");
        assertEquals(4,length);

        System.out.println(context.read("$..id").toString()); //[1001,1002,1003,1004]
        System.out.println(context.read("$.[0:1]").toString());
        System.out.println(context.read("$.[?(@.name=='iTEM 1')]").toString());
        System.out.println(context.read("$.[?(@.quantity==8)]").toString());
        System.out.println(context.read("$..id").toString());
    }
}
