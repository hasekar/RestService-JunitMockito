package com.restMokitoUnitTest.kata.RestServicetest;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class HamsterAssertTest {

    @Test
    public void learning() {
        List<Integer> listIntegers = Arrays.asList(2, 3, 4);

        assertThat(listIntegers, hasSize(3));
        assertThat(listIntegers,hasItems(2,3));
        assertThat(listIntegers,everyItem(greaterThan(1)));
        assertThat(listIntegers,everyItem(lessThan(5)));
        assertThat("",isEmptyString());
        assertThat("ABCDE",containsString("BCD"));

    }
}
