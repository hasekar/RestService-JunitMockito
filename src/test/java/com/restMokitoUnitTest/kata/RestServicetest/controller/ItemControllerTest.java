package com.restMokitoUnitTest.kata.RestServicetest.controller;

import com.restMokitoUnitTest.kata.RestServicetest.Entity.Item;
import com.restMokitoUnitTest.kata.RestServicetest.domain.ItemController;
import com.restMokitoUnitTest.kata.RestServicetest.service.ItemService;
import com.restMokitoUnitTest.kata.RestServicetest.service.ItemServiceModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ItemController.class)
public class ItemControllerTest {

    @Autowired
    MockMvc mockMvc;


    @MockBean
    ItemService itemServiceModel;

    @Test()
    public void testgetItemfromDAO() throws Exception {

        when(itemServiceModel.retriveAllItemfromDAO()).thenReturn(Arrays.asList(new Item(1001, "iTEM 1",10,0)));

        RequestBuilder request = MockMvcRequestBuilders
                .get("/item-dao")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json("[{ \"id\":1001,\"name\":\"iTEM 1\",\"price\":10,\"quantity\":10,\"value\":0}]"))
                .andReturn();
    }
    @Test
    public void testItemController() throws Exception {



        RequestBuilder request = MockMvcRequestBuilders
                .get("/item")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult =  mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"name\":\"Item 1\",\"price\":10,\"quantity\":100}"))
                .andReturn();
    }

    @Test
    public void testItemControllerWithItemService() throws Exception {

        when(itemServiceModel.retrieveItem())
                .thenReturn(new Item(1,"Item 1",10,100));

        RequestBuilder request = MockMvcRequestBuilders
                .get("/item-from-service")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult =  mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"name\":\"Item 1\",\"price\":10,\"quantity\":100}"))
                .andReturn();
    }
}
