package com.restMokitoUnitTest.kata.RestServicetest.DAO;

import com.restMokitoUnitTest.kata.RestServicetest.Entity.Item;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TestDAO {

    @Autowired
    ItemDAOModel itemDAOModel;

    @Test
    public void testDAO() {
        List<Item> itemList = itemDAOModel.findAll();
        assertEquals("iTEM",itemList.get(0).getName());
    }

}
