package com.restMokitoUnitTest.kata.RestServicetest;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class AssertJTest {

    @Test
    public void learning() {
        List<Integer> listIntegers = Arrays.asList(2, 3, 4);

//        assertThat(listIntegers, hasSize(3));
        assertThat(listIntegers).hasSize(3)
                .contains(2)
                .allMatch(x -> x>1)
                .allMatch(x -> x <5);
//        assertThat(listIntegers,hasItems(2,3));
//        assertThat(listIntegers,everyItem(greaterThan(1)));
//        assertThat(listIntegers,everyItem(lessThan(5)));
//        assertThat("",isEmptyString());
//        assertThat("ABCDE",containsString("BCD"));

    }
}
