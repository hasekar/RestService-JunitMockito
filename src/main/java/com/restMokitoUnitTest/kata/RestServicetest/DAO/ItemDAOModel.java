package com.restMokitoUnitTest.kata.RestServicetest.DAO;

import com.restMokitoUnitTest.kata.RestServicetest.Entity.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ItemDAOModel extends JpaRepository<Item, Integer> {

}
