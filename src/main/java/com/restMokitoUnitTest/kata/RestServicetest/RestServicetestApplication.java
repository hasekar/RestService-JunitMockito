package com.restMokitoUnitTest.kata.RestServicetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestServicetestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestServicetestApplication.class, args);
	}
}
