package com.restMokitoUnitTest.kata.RestServicetest.domain;

import com.restMokitoUnitTest.kata.RestServicetest.Entity.Item;
import com.restMokitoUnitTest.kata.RestServicetest.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ItemController {

    @Autowired
    ItemService itemSerice;

    @GetMapping("/item")
    public Item getItem(){
        return new Item(001,"Item 1", 10, 100);
    }

    @GetMapping("/item-dao")
    public List<Item> getItemfromDAO(){
        return itemSerice.retriveAllItemfromDAO();
    }

    @GetMapping("/item-from-service")
    public Item getItemfromService(){
        return itemSerice.retrieveItem();
    }
}
