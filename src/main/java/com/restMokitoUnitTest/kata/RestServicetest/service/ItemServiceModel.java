package com.restMokitoUnitTest.kata.RestServicetest.service;

import com.restMokitoUnitTest.kata.RestServicetest.Entity.Item;

import java.util.List;

public interface ItemServiceModel {
    Item retrieveItem();
    List<Item> retriveAllItemfromDAO();
}
