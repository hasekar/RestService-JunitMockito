package com.restMokitoUnitTest.kata.RestServicetest.service;

import com.restMokitoUnitTest.kata.RestServicetest.DAO.ItemDAOModel;
import com.restMokitoUnitTest.kata.RestServicetest.Entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemService implements ItemServiceModel{

    @Autowired
    ItemDAOModel itemDAO;

    @Override
    public Item retrieveItem() {
        return new Item(1, "Item 1", 10, 100);
    }

    @Override
    public List<Item> retriveAllItemfromDAO() {

        List<String> list = itemDAO.findAll().stream().map(Item::getName).collect(Collectors.toList());

        return itemDAO.findAll().stream().map(i -> {
                    i.setValue(i.getPrice()*i.getQuantity());
                    return i;
        }).collect(Collectors.toList());
    }
}
